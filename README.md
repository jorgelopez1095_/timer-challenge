# T I M E R | challenge

Challenges: concurrency, competing user/signal interactions, responsiveness.
The task is to build a frame containing a gauge G for the elapsed time e, a label which shows the elapsed time as a numerical value, a slider S by which the duration d of the timer can be adjusted while the timer is running and a reset button R. Adjusting S must immediately reflect on d and not only when S is released. It follows that while moving S the filled amount of G will (usually) change immediately. When e ≥ d is true then the timer stops (and G will be full). If, thereafter, d is increased such that d > e will be true then the timer restarts to tick until e ≥ d is true again. Clicking R will reset e to zero.
[DOWNLOAD APK](https://drive.google.com/file/d/1XWZXxf8PG8LSO7gn6VVCevY8ySZq6m1P/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/timer_i.jpeg "Main-view application")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [ViewBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

- N/A Just activity and view.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).