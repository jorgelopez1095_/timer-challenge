package com.hugo.challenge.timer

import android.animation.ValueAnimator
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.hugo.challenge.timer.databinding.ActivityTimerBinding

class TimerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTimerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTimerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonReset.setOnClickListener {
            loadFeature()
        }
    }

    private fun loadFeature() {
        val animator = ValueAnimator.ofInt(0, 30)
        animator.duration = 10000
        binding.seekBarController.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val formula = (1000 * progress).toLong()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        var x = 0
        animator.addUpdateListener { animation ->
            x++
            binding.textViewSeconds.text = "${animation.animatedValue}.0s"
            binding.progressBarTimer.progress = animation.animatedValue.toString().toInt()
        }
        animator.start()
    }
}